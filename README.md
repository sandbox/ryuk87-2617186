# WHISKY
## A Drupal 8 starter kit theme based on Bourbon, Neat and Bitters. Only for developers ;)
---
### Features:
* [Bourbon sass library](http://bourbon.io/)
* [Neat grid system](http://neat.bourbon.io/)
* [Bitters base styles](http://bitters.bourbon.io/)
* Direct sass compilation with [scssphp](http://leafo.net/scssphp/)
---
#### Todo:
* Drush commands (recompile sass, empty Drupal cache automatically,...)
* Alternative sass compilation (Gulp?)
---

I'm open to any suggestions/comments/new features! :)
